<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>醫脈達平台接口调试工具</title>
    <head>
<body>
<div id="main" class="main">
    <h1>醫脈達平台接口调试工具</h1>

    <div class="panel">
        <div id="description" class="des">
            此工具旨在帮助开发者检测调用【微信公众平台开发者API】时发送的请求参数是否正确，提交相关信息后可获得服务器的验证结果				</div>

        <ol id="manual" class="manual">
            <li><b>使用说明：</b></li>
            <li>（1）选择合适的接口。</li>
            <li>（2）系统会生成该接口的参数表，您可以直接在文本框内填入对应的参数值。（红色星号表示该字段必填）</li>
            <li>（3）点击检查问题按钮，即可得到相应的调试信息。</li>
        </ol>

        <div id="content" class="content">
            <div id="typeSelectorDiv" class="frm_control_group">
                <label class="frm_label">一、接口类型：</label>
                <div class="frm_controls">
                    <select id="typeSelector" class="frm_input_box">
                    </select>
                </div>
            </div>
            <div id="formSelectorDiv" class="frm_control_group">
                <label class="frm_label">二、接口列表：</label>
                <div class="frm_controls">
                    <select id="formSelector" class="frm_input_box">
                    </select>
                    <span id="methodType" class="frm_tips">方法：GET</span>
                </div>
            </div>
            <div id="formContent" class="frm_control_group">
                <label>三、参数列表：</label>
                <br /><br />
                <div id="formContainer">

                </div>
            </div>
        </div>


        <div id="resultContainer"></div>
        <div class="iframe-container hide">
            <iframe frameborder="no" scrolling="no" marginheight="0" marginwidth="0" id="result-iframe" name="result-iframe"></iframe>
        </div>
    </div>
</div>
</body>
</html>
