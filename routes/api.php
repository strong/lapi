 <?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([

    'middleware' => 'api',
    //'prefix' => 'auth'

],
    function ($router) {
        Route::get('video/first', 'Api\VideoController@first');
        Route::get('video/lists', 'Api\VideoController@lists');
    });
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     Route::get('video/first', 'Api\VideoController@first');
//     //return $request->user();
// });

