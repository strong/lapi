<?php

namespace App\models;

use App\Models\BaseModel;

class Video extends BaseModel
{
    protected $table = 'ims_y_med_video';

    public static function findData($id=''){
        return self::find($id);
    }

    public static function videos($attributes)
    {
        extract($attributes);
        $id = isset($id) ? $id : '';
        $where = array('status' => '上架', 'is_bear' => 1);
        if(!empty($id)){
            $where['id'] = $id;
        }
        $model = self::where($where);
        //, 'paged' => self::formatPaged($page, $per_page, $total)
        //where($type, 1)->->get()
        $data = $model->where($where)->orderBy('browsenum','desc')->orderBy('id', 'desc')->paginate($per_page)->toArray();
        if(empty($data['data'])) return self::formatError(self::BAD_REQUEST, '没有数据');

        return self::formatBody(['videos'=>$data['data']]);
    }

    public static function findAll(array $attributes)
    {
        extract($attributes);


        $model = self::where(['status' => '上架', 'is_bear' => 1]);

        $total = $model->count();

        $data = $model
            ->orderBy('id', 'ASC')->orderBy('id', 'DESC')
            ->paginate($per_page)->toArray();

        return self::formatBody(['paged' => self::formatPaged($page, $per_page, $total),'videos' => $data['data']]);
    }
  
}
